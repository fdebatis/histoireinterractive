#!/bin/bash
clear
# Initialise the Title Art
file1="./chapitre3.ben"
while IFS= read -r line
do
   echo "$line"
done <"$file1"

    sleep 1
    echo "L'expérience dans la société Google ayant échouée, vous êtes au chômage"
    echo "Est-ce que vous retournez chez vos parents?"
    echo "A pour oui, B pour non"
    sleep 1
    choixa0=-1
    while [ $choixa0 != A ] && [ $choixa0 != B ]; do
        read -p "> " choixa0
        case $choixa0 in
        A ) sleep 1
            echo "Vous êtes chez vos parents comme un 'Tanguy'. Supportent-ils la situation ?"
            echo "A pour oui, B pour non"
            choixa1=-1
            sleep 1
            while [ $choixa1 != A ] && [ $choixa1 != B ]; do
                read -p "> " choixa1
                case $choixa1 in
                A ) sleep 1
                    echo "Vous passez vos journées à naviguer sans but sur votre PC en espérant connaître une nouvelle aventure palpitante... Vous vous satisfaisez de votre situation mais c'est vraiment pas ouf.";;
                B ) sleep 1
                    echo "Vos parents vous obligent à ambitionner de travailler. Préfèrez-vous reprendre des études ou retourner dans la vie active?"
                    echo "A pour les études, B pour la vie active"
                    choixa2=-1
                    sleep 1
                    while [ $choixa2 != A ] && [ $choixa2 != B ]; do
                        read -p "> " choixa2
                        case $choixa2 in
                        A ) sleep 1
                            echo "Vous décidez d'entamer une licence d'informatique et espérer intégrer ensuite l'UTC. Beau projet!";;
                        B ) sleep 1
                            echo "Vous devenez employé chez McDonald's. Cela pourra au moins vous permmettre d'ajouter une ligne à votre CV... ";;
                        * ) echo "Desole je vous comprend pas, les choix sont A et B.";; #Voici le cas par defaut si l'utilisateur de rentre pas la bonne chose
                        esac
                    done;;
                * ) echo "Desole je vous comprend pas, les choix sont A et B.";; #Voici le cas par defaut si l'utilisateur de rentre pas la bonne chose
                esac
            done;;

        B ) sleep 1
            echo "Etes-vous à la recherche d'un emploi?"
            echo "A pour oui, B pour non"
            choixa3=-1
            sleep 1
             while [ $choixa3 != A ] && [ $choixa3 != B ]; do
                read -p "> " choixa3
                case $choixa3 in
                 A ) sleep 1
                    echo "Cherchez-vous pour travailler dans une start-up ou dans une grande société?"
                     echo "A pour oui, B pour non"
                     choixa4=-1
                     sleep 1
                     while [ $choixa4 != A ] && [ $choixa4 != B ]; do
                        read -p "> " choixa4
                        case $choixa4 in
                        A ) sleep 1
                            echo "Vous avez été dégoûté par Google et voulez travailler dans une petite start-up en plein essor. Vous aurez sans doute un emploi épanouïssant! ";;
                        B ) sleep 1
                            echo "Vous êtes déterminé à prendre part à d'immenses projets. Travailler avec un rythme d'enfer ne vous fait pas peur et l'éthique vous importe peu. ";;
                        * ) echo "Desole je vous comprend pas, les choix sont A et B.";; #Voici le cas par defaut si l'utilisateur de rentre pas la bonne chose
                        esac
                     done;;
                 B ) sleep 1
                    echo "Restez-vous dynamique ?"
                     echo "A pour oui, B pour non"
                     choixa5=-1
                     sleep 1
                     while [ $choixa5 != A ] && [ $choixa5 != B ]; do
                        read -p "> " choixa5
                        case $choixa5 in
                        A ) sleep 1
                            echo "Vous restez à l'affût des dernières infos sur le monde des logiciels propriétaires et passez votre temps à comprendre leur fonctionnement. Vous accumulez une vaste culture sur le sujet. Cela vous sera utile à coup sûr!";;
                        B ) sleep 1
                            echo "Vous passez vos journées à naviguer sans but sur votre PC en espérant connaître une nouvelle aventure palpitante... Vous vous satisfaisez de votre situation mais c'est vraiment pas ouf.";;
                        * ) echo "Desole je vous comprend pas, les choix sont A et B.";; #Voici le cas par defaut si l'utilisateur de rentre pas la bonne chose
                        esac
                     done;;
                 * ) echo "Desole je vous comprend pas, les choix sont A et B.";; #Voici le cas par defaut si l'utilisateur de rentre pas la bonne chose
                 esac
            done;;
        * ) echo "Desole je vous comprend pas, les choix sont A et B.";; #Voici le cas par defaut si l'utilisateur de rentre pas la bonne chose
        esac
    done

