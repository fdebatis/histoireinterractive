#!/bin/bash

clear
# Initialise the Title Art
file1="./chapitre2.ben"
while IFS= read -r line
do
   echo "$line"
done <"$file1"


choix=-1
rep1=0
rep2=0
rep3=0
if [ $1 = 1 ]
then
	#embauché chez google en tant que que stagiaire
	echo -e "Le début de ton rêve commence enfin, plus tu y pensais plus l'idée d'y travailler te réjouissais! c'est un travail ambitieux mais pleins de challenge !\nEn arrivant, tout le monde t’accueil gentillement, te font visiter les lieux et ton nouveau boss te présente ton bureau et les tâches qui t’attendent…\nTu te mets finalement à travailler, ce travail est vraiment intéressant bien qu’épuisant.\n"
	sleep 4

	#CHOIX !
	echo -e "Tes nouveaux collègues te proposent un café, une petite pause ne serait pas de refus…\n"
	sleep 3
	echo "Entrez votre réponse: A ou B"
	echo -e "\n"
	echo -e "   -A : Bien sur c’est une bonne occasion de faire connaissance !\n   -B : Je dois bosser…\n"
	while [ $choix != A ] && [ $choix != B ]; do
	    read -p "> " choix
	    case $choix in
	        A ) rep1=A;;
	        B ) rep1=B;;
	        * ) echo "Desole je vous comprend pas, les choix sont A et B.";; #Voici le cas par defaut si l'utilisateur de rentre pas la bonne chose
	    esac
	done
	choix=-1

	echo -e "Après cette première journée épuisante tu rentres chez toi pour une bonne nuit de sommeil…\n"
	sleep 2
	#CHOIX !
	echo -e "Deux jours plus tard, il est 15h, le ventre vide, tu n’avais pas faim ce midi, une pause café te fais rêver, c’est justement l’heure où tu pourrait croiser tes collègues préférés !\n"
	sleep 3
	echo -e "Entrez votre réponse: A ou B\n"
	echo -e "   -A : goooo !\n   -B : Non, un peux de professionnalisme ! (j’ai faim p*tain)\n"
	while [ $choix != A ] && [ $choix != B ]; do
	    read -p "> " choix
	    case $choix in
	        A ) rep2=A;;
	        B ) rep2=B;;
	        * ) echo "Desole je vous comprend pas, les choix sont A et B.";; #Voici le cas par defaut si l'utilisateur de rentre pas la bonne chose
	    esac
	done
	choix=-1

	echo -e "Cette dalle d’enfers te fit rentrer le plus vite possible à la maison, beaucoup trop manger au repas, tu t’endors comme une masse…\n"
	echo -e "LE REVEIL N’A PAS SONNE !! Tu n’es pas trop en retard mais tu n’as plus d’avance non plus !\nSi tu pars dans 5 minute tu devrais pouvoir arriver à l’heure…\n"
	sleep 3
	#CHOIX !
	echo -e "Est ce que tu cours pour être sûr d’être à l’heure même si tu vas finir épuisé et la journée risque d’être looongue… ?\n"
	echo -e "Entrez votre réponse: A ou B\n"
	echo -e "   -A : pas la peine normalement je serais quand même à l’heure\n   -B : COURS, COURS, COURS !!!\n"
	while [ $choix != A ] && [ $choix != B ]; do
	    read -p "> " choix
	    case $choix in
	        A ) rep3=A;;
	        B ) rep3=B;;
	        * ) echo "Desole je vous comprend pas, les choix sont A et B.";; #Voici le cas par defaut si l'utilisateur de rentre pas la bonne chose
	    esac
	done
	echo -e "La première semaine aura été difficile, la suite aussi mais tellement stimulante que tu as su directement que c'était le boulot de ta vie. \nAujourd'hui, ton contract prend fin, tu serais ravi de continer dans cette entreprise\n"
	sleep 3
	# 2 ou 3 bons choix 
	if [ $rep1 = B ] && [ $rep2 = B ] 
	then
		echo -e "* Le jour J * \nTon boss est une vrai pipelette alors moi et ma divine bonté avons décidé de te faire un résumé TRES COURT de la conversation : \nBOSS : Vous bossez bien, chaud pour continuer avec nous ?\nTOI : Yup\nCool !! Ton premier CDI ! (j'avais dit que c'était court)"
		r=1
	else
		if [ $rep1 = B ] && [ $rep3 = B ]
		then
			echo -e "* Le jour J * \nTon boss est une vrai pipelette alors moi et ma divine bonté avons décidé de te faire un résumé TRES COURT de la conversation : \nBOSS : Vous bossez bien, chaud pour continur continuer avec nous ?\n"
			echo -e "TOI : Yup\nCool !! Ton premier CDI ! (j'avais dit que c'était court)\n"
			r=1
		else
			if [ $rep2 = B ] && [ $rep3 = B ] 
			then
				echo -e "* Le jour J * \nTon boss est une vrai pipelette alors moi et ma divine bonté avons décidé de te faire un résumé TRES COURT de la conversation : \n"
				echo -e "BOSS : Vous bossez bien, chaud pour continuer avec nous ? \nTOI : Yup\nCool !! Ton premier CDI ! (j'avais dit que c'était court)\n>"
				r=1
			else
				if [ $rep1 = B ] && [ $rep2 = B ] &&  [ $rep3 = B ]
				then
					echo -e "* Le jour J * \nTon boss est une vrai pipelette alors moi et ma divine bonté avons décidé de te faire un résumé TRES COURT de la conversation : \n"
		                        echo -e "BOSS : Vous bossez bien, chaud pour continuer avec nous ? \nTOI : Yup\nCool !! Ton premier CDI ! (j'avais dit que c'était court)\n>"
					r=1
				else	#2 ou 3 erreurs
					echo -e "* Le jour J * \nTu entres, confiant, fière de ta progression dans ton travail.\nBOSS : Je ne passerais pas par quatre chemins, je vous ai plus vu à la machine à café qu’a votre bureau et vous étiez le premier parti et le dernier arrivé.\n Vous avez pris de mauvaises habitude, c’est pourquoi je vais maintenant mettre fin à notre contrat.\nTout s’écroule autour de toi… Tu ne t’attendais en aucun cas à ça, tu venais de perdre le travail de ta vie…\nTu n’arriveras jamais à passer à autre chose, en dépression tu décidas de te pendre 2 mois plus tard..\n"
					sleep 5
					echo -e "\n"
					echo "GAME OVER"
					exit
				fi
			fi
		fi
	fi
	sleep 3
	if [ $r = 1 ]
	then 
		./chapitre3_2P.sh
		b=0
	else 
		b=1
	fi
else
	#Developpe une obsession pour google
	choix=-1
	a=0
	echo -e "Après avoir fait toute tes recherches tu as fini par te passionner pour google. \nLe monde de google, la technologie et le progrès te passionne.\nTu t'intéresses aussi au PDG, sur le point de vue son raisonnement, de son parcours puis de sa vie privée\n"
	sleep 2
	echo -e "Ce personnage te fascine, tu t'informes par les journeaux de sa vie, s'il est marié, avec des enfants, où il habite...\n"
	echo -e "L'idée d'un entretien avec lui te fait rêver, non seulement pour l'opportunité professionnelle, mais aussi pour rencontrer cet homme que tu admires tant..."
	sleep 3
	echo -e "\nHier tu as été informé de sa venue d'aujourd'hui dans une ville a moins de 1h de route de chez toi, ce matin tu dois faire un choix important:\n"
	echo -e "Entrez votre réponse: A ou B\n"
	echo -e "   -A: inutile de se déplacer, il sera occupé je ne pourrais pas lui parler ni réussir à obtenir un entretien personnel\n"
	echo -e "   -B: C'est ma chance! Je n'ai jamais été aussi proche de lui, même si je n'aurais pas l'occasion de lui parler je pourrait au moins voir mon idole!\n"
	while [ $choix != A ] && [ $choix != B ]; do
	    read -p "> " choix
	    case $choix in
	        A ) a=1;;
	        B ) a=2;;
	        * ) echo "Desole je vous comprend pas, les choix sont A et B.";; #Voici le cas par defaut si l'utilisateur de rentre pas la bonne chose
	    esac
	done

	if [ $a = 1 ]
	then
		echo -e "Rester à la maison est plus raisonnable, tu as raison, tu travailles donc plutôt tes recherches sur google pour pouvoir être lors de ta réelle occasion\n"
		echo -e "Mortifié de le savoir proche, tu travailles dur jusqu'à l'épuisement\n"
		sleep 3
		echo -e "Proche de l'endormissement une notification t'interpelle sur ton téléphone: Un message d'un membre de google t'ayant remarqué sur le fan account de google que tu as créé!\n"
		echo -e "Il te propose un rendez vous pour faire connaissance et pouvoir discuter d'opportunité professionnelle! Le rêve!!\n"
		sleep 2
		echo -e "Tu te rends immédiatement sur place, ils t'acceuillent comme un roi! Tu retrouves enfin l'employé t'ayant contacté. Il te fait entrer dans un bureau mais contre toutes attentes ce n'est pas le sien\n"
		echo -e "Il te laisse quelques instants et LE PDG DE GOOGLE ENTRE DANS LE BUREAU!! c'était en faite SON bureau!!\n"
		echo -e "Il te propose une place EXTRAORDINAIRE!! Tu l'accompagneras en tant que PDG !! \n"
		sleep 2
		echo -e "Tu te mets à pleurer, tu viens d'optenir tout ce dont tu as toujours rêvé, dans l'agitation et l'hystérie quelque chose d'innespéré arriva...\n"
		sleep 2
		echo -e "Ce dont tu rêvais secrètement...\n"
		sleep 4
		echo -e "le PDG s'approche de toi doucement...\n"
		sleep 6
		echo -e "...Il t'embrasse\n"
		sleep 2
		echo -e "\n C'est par le bruit de l'aspirateur de ta maman que tu es soudain réveillé... C'était le plus beau des rêves que tu ais fait..."
		sleep 10
		./chapitre3_1P.sh
	else
		echo -e "Tu as raison passons à l'action!! \nTu te rends au lieu indiqué sur sa page facebook \n"
		echo -e "Sur les lieux, tu le remarques enfin. Il est là en face de toi. Mais il ne te remarque pas.\n"
		sleep 2
		echo -e "Tu entends qu'il cherche quelqu'un de passionné et motivé pour un nouveau post très important\nC'est la chance de ta vie!!!\n"
		sleep 1
		echo -e "Pourtant il annonce qu'il n'était là que pour quelques heures et qu'il s'apprète à prendre son jet privé pour retourner chez lui\n"
		sleep 2
		echo -e "Tu prends très vite une décision, pour lui prouver ta motivation et ta passion tu décides de monter en cachette dans son jet\n"
		echo -e "Tu réussis (par je ne sais par quelle magie) à monter. Depuis la soute aucune occasion de lui parler, tu décides donc d'aller au bout de tes idées...\n"
		sleep 2
		echo -e  "Enfin arrivé tu décides de t'introduire chez lui (car tu connais son adresse par coeur) pour pouvoir décrocher ce poste\n"
		echo -e "Tu prends donc les devants, tu te dépêches d'entrer chez lui, ce fut facile tu avais déjà réfléchie à comment faire ça.\nL'étape suivante est d'attendre qu'il arrive\n"
		sleep 4
		echo -e "Le temps passe, il n'est toujours pas rentré. Tu t'inquiètes beaucoup pour lui quand tout à coup tu entends le bruit des voitures de police\n"
		sleep 2
		echo -e "Tu t'étais fait repéré par ses voisins\n"
		echo -e "\nTu finiras ta vie en prison pour avoir stalké et commis une effraction chez un homme aussi riche et influent que ce PDG qui t'inspirait tant"
		sleep 5
		echo -e "\n\nGAME OVER"
		exit
	fi
fi
