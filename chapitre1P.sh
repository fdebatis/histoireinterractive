#!/bin/bash
clear
# Initialise the Title Art
file1="./chapitre1.ben"
while IFS= read -r line
do
   echo "$line"
done <"$file1"
 
choix1=-1
choix2=-1
choix3=-1
choix4=-1
choix5=-1

 
echo "Vous avez à peine fait votre choix que vous voyez la réponse du hacker apparaître sous vos yeux..."
sleep 3
echo '"Vous avez donc choisi les logiciels propriétaires !"'
echo '"Que le sort vous soit favorable !"'
echo 
sleep 4
echo "Heureux d'être enfin tranquille, vous finissez tranquillement votre soirée en repensant à cet étrange échange..."
echo "Au moins, votre ordinateur étant déjà sous windows vous n'aurez pas besoin de vous prendre la tête pour changer le système d'exploitation !"
echo
sleep 4
echo "Un peu fatigué par votre journée, vous allez vous coucher..."
sleep 4
echo "Vous vous réveillez avec une énergie incroyable, préférez-vous petit déjeuner ou directement vous lancer dans vos recherches de stage de fin d'étude ?"
echo "Saisissez A pour prendre votre petit dej, B pour entamer vos recherches de stage"
echo
 
while [ $choix1 != A ] && [ $choix1 != B ]; do
   read -p "> " choix1
   case $choix1 in
       A ) echo "Premier réflexe de votre journée, vous vous faites un petit café et en tant que bon français(e) et attrapez un magnifique croissant";;
       B ) echo "Un magnifique croissant vous fait de l'oeil mais il attendra !";;
       * ) echo "Désolé je vous comprend pas, les choix sont A et B.";; #Voici le cas par defaut si l'utilisateur de rentre pas la bonne chose
   esac
done
 
sleep 3
echo
echo "Vous vous installez devant votre magnifique ordinateur a licence propriétaire et vous scrollez les différentes offres qui défilent sous vos yeux."
sleep 3
echo 'Soudain ! Une certaine offre attire votre attention, stagiaire cloud chez une certaine entreprise: "Google"'
echo "Ce nom vous dit vaguement quelque chose, peut-être une petite startup installée pas loin de votre domicile ?"
sleep 4
echo "Une chose est sûre, la deadline pour trouver votre stage se rapproche dangereusement, comptez-vous candidater ou continuer vos recherches en ignorant cette opportunité?"
sleep 4
echo "Saisissez A pour candidater et B pour continuer vos recherches"
echo
 
while [ $choix2 != A ] && [ $choix2 != B ]; do
   read -p "> " choix2
   case $choix2 in
       A ) echo "Le descriptif de l'offre match vos attentes, ni une, ni deux, vous envoyez votre magnifique CV et votre incroyable lettre de motivation !";;
       B ) echo "Vos ambitions vont bien au-delà qu'une aussi petite entreprise, vous continuez donc vos recherches..."
           sleep 3
           echo "Malheureusement, vous avez été trop sélectif et malgré des dizaines de candidatures spontanées envoyées à la volée vous n'avez pas de réponse avant la date limite posée par votre université..."
           echo "Celle-ci décide donc de vous virer !"
           echo
           sleep 5
           echo "Vous passerez le restant de vos jours à étudier afin de devenir avocat et attaquer en justice votre université. Malheureusement, ce plan échouera..."
           echo "Game over..."
           exit;;
       * ) echo "Désolé je vous comprend pas, les choix sont A et B.";; #Voici le cas par defaut si l'utilisateur de rentre pas la bonne chose
   esac
done

echo
echo "Coup de chance ! Les RHs, comme d'habitude, sont SUPER efficaces et vous avez à peine le temps de finir de boire votre café que vous avez déjà une réponse dans votre boite mail !"
echo "Vous avez un entretien cette même après-midi, souhaitez vous préparer l'entretien ou sortir prendre l'air ?"
sleep 4
echo
echo "Tapez A pour preparer l'entretien, B pour sortir prendre l'air"
echo
 
while [ $choix3 != A ] && [ $choix3 != B ]; do
   read -p "> " choix3
   case $choix3 in
       A ) echo "Vous vous mettez à étudier l'entreprise de fond en comble, sa création, son PDG, son chiffre d'affaires, plus rien n'a de secret pour vous !"
           echo "Vous arrivez à l'entretien un peu stressé, mais au moins vous avez bien préparé !"
           sleep 3;;
       B ) echo "Vous profitez de cet air frais, rien de mieux que de passer l'entretien en restant naturel en pleine improvisation !"
           echo "Vous arrivez à l'entretien un peu stressé, mais vous vous faites confiance, vous arriverez bien à improviser quelque chose de bien !"
           sleep 3;;
       * ) echo "Désolé je vous comprend pas, les choix sont A et B.";; #Voici le cas par defaut si l'utilisateur de rentre pas la bonne chose
   esac
done
 
echo
echo "RH- Bonjour, l'entretien pour notre entreprise prestigieuse est un véritable défis, êtes-vous prêt à commencer ?"
sleep 4
echo "Tapez A si oui, B sinon"
echo
 
while [ $choix4 != A ]; do
   read -p "> " choix4
   case $choix4 in
       A ) echo "Vous confirmez votre determination, vous allez surmonter tous les defis !";;
       B ) echo "Vous souhaitez fuire et prendre vos jambes à votre cou mais votre morale vous l'empêche, vous prenez donc votre respiration et demandez au RH de reposer sa question"
           echo "RH- Êtes-vous prêt à commencer ?"
           echo "Tapez A si oui, B sinon";;
       * ) echo "Désolé je vous comprend pas, les choix sont A et B.";; #Voici le cas par defaut si l'utilisateur de rentre pas la bonne chose
   esac
done
 
echo
echo "RH- Bon, je vais vous poser une et une seule question qui déterminera votre candidature. "
sleep 4
echo "Quel est le résultat de 2+1 ?"
echo "Saisissez A pour repondre 3, B pour répondre 4 et C pour toute autre réponse."
echo

while [ $choix5 != A ] && [ $choix5 != B ] && [ $choix5 != C ]; do
   read -p "> " choix5
   case $choix5 in
       A ) echo '"Confiant, vous répondez en un instant: "TROIS !!"'
           sleep 3
           echo
           echo "La RH s'étonne de la bonne réponse et vous applaudit !"
           echo "RH- Felicitation ! Vous êtes notre premier candidat cette année à trouver la bonne réponse ! Vous etes pris !"
           sleep 3
           echo "Vous avez prouvé votre excellence et l'opportunité de rejoindre les rangs du logiciel propriétaire s'offre à vous !"
           echo "BRAVO !"
           ./chapitre2P.sh 1;;
       B ) echo '"Confiant, vous répondez en un instant: "QUATRE !!"'
           echo
           echo "La RH semble ne pas être surprise par votre réponse et vous dit: "
           echo "RH- Vous avez répondu comme tous les candidats que nous avons pu avoir en entretien cette année, peut être que nos conditions d'entrée sont trop exigeantes mais je ne peux malheureusement pas vous accepter..."
           sleep 5
           echo "Un orchestre apparaît par magie derrière vous et se met à jouer une musique triste..."
           sleep 5
           case $choix3 in
               A ) echo "Vous aviez pourtant si bien préparé cet entretien... Quel dommage ! Comment est ce que cela a bien pu se produire ?!"
                   echo "Vous vous jurez de ne plus jamais vous faire avoir de la même façon. Toutes vos prochaines semaines, tous vos prochains mois, toutes vos prochaines années seront dédiées à TOUT connaître sur Google !";;
               B ) echo "Cette confiance en vous vous aura coûté chers ! Il faut croire que se balader dehors au lieu de préparer l'entretien n'était peut être pas la bonne solution..."
                   echo "Quoi qu'il en soit, le goût de cette défaite est amer !"
                   echo "Vous vous jurez de ne plus jamais vous faire avoir de la même façon. Toutes vos prochaines semaines, tous vos prochains mois, toutes vos prochaines années seront dédiées à TOUT connaître sur Google !";;
           esac
           sleep 10
           ./chapitre2P.sh 2;;
       C ) echo '"Confiant, vous criez votre réponse !"'
           echo
           echo "La RH semble ne pas être surprise par votre réponse et vous dit: "
           echo "RH- Vous avez répondu comme tous les candidats que nous avons pu avoir en entretien cette année, peut être que nos conditions d'entrée sont trop exigeantes mais je ne peux malheureusement pas vous accepter..."
           sleep 5
           echo "Un orchestre apparaît par magie derrière vous et se met à jouer une musique triste..."
           sleep 2
           case $choix3 in
               A ) echo "Vous aviez pourtant si bien préparé cet entretien... Quel dommage ! Comment est ce que cela a bien pu se produire ?!"
                   echo "Vous vous jurez de ne plus jamais vous faire avoir de la même façon. Toutes vos prochaines semaines, tous vos prochains mois, toutes vos prochaines années seront dédiées à TOUT connaître sur Google !";;
               B ) echo "Cette confiance en vous vous aura coûté chers ! Il faut croire que se balader dehors au lieu de préparer l'entretien n'était peut être pas la bonne solution..."
                   echo "Quoi qu'il en soit, le goût de cette défaite est amère !"
                   echo "Vous vous jurez de ne plus jamais vous faire avoir de la même façon. Toutes vos prochaines semaines, tous vos prochains mois, toutes vos prochaines années seront dédiées à TOUT connaître sur Google !";;
           esac
           sleep 10
           ./chapitre2P.sh 2;;
       * ) echo "Désolé je vous comprend pas, les choix sont A, B et C.";; #Voici le cas par defaut si l'utilisateur de rentre pas la bonne chose
   esac
done

