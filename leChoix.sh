#!/bin/bash
clear
# Initialise the Title Art
file1="./introduction.ben"
while IFS= read -r line
do
   echo "$line"
done <"$file1"

#PREMIER CHOIX (sans conséquence)

echo -e "YOU : C’est un jour comme un autre, un jour rempli de travail, de fou rire et de projets qui avancent entre deux bières au PIC.\nJ’allume mon ordi pour pouvoir faire ce que j’aime le plus..."
sleep 3
echo -e "\n-----------\nA:Travailler comme un.e bon.ne étudiant.e et future ingénieur.e que je suis\nB:Regarder les bons vieux mêmes dans UTC :( (ils sont quand mêmes des génies!)\nC:Regarder Netflix, après l’effort, le réconfort!\n-----------"
choix=-1
while [ $choix != A ] && [ $choix != B ] && [ $choix != C ]; do
    read -p "votre choix (A/B/C): " choix 
    case $choix in
        A ) echo "YOU : Travailler comme un.e bon.ne étudiant.e et future ingénieur.e que je suis" ;;
        B ) echo "YOU : Regarder les bons vieux mêmes dans UTC :( (ils sont quand même des génies!)" ;;
        C ) echo "YOU : Regarder Netflix, après l’effort, le réconfort!" ;;
	* ) echo "Hey, tu as pas marqué un choix existant! Tapes A,B ou C";; #Voici le cas par defaut si l'utilisateur de rentre pas la bonne chose
    esac
done

A="Travailler comme un.e bon.ne étudiant.e et future ingénieur.e que je suis"
B="Regarder les bons vieux mêmes dans UTC :( (ils sont quand mêmes des génies!)"
C="Regarder Netflix, après l’effort, le réconfort!"

echo -e "\n"

function troischoix  {
	 if [ $choix = "A" ]
        then
                echo "$A"
        elif [ $choix = "B" ]
        then
                echo "$B"
        else
                echo "$C"
        fi
}






function deuxchoix {

        if [ $choix = "A" ]
        then
                echo "$SUJET : $A"
        else
                echo "$SUJET : $B"
        fi
}




echo ""
sleep 2 
echo "Je m’installe confortablement, ready pour une soirée de folie quand tout à coup…" 
echo -e "\n"
sleep 2 
echo "... : Yo!"
echo -e "\n"
sleep 2 
echo "YOU : ??"
echo -e "\n"
sleep 2 
echo "... : Rhalala franchement c’était vraiment trop facile de prendre toutes tes données! MOUHAHA!"
echo -e "\n"
sleep 2 
echo "YOU : Attends mais qui es tu? Mon ordi parle? Désolée mec t’as du voir tellement de choses…"
echo -e "\n"
sleep 2 
echo "HACKER : Si seulement c’était ton ordi… Non désolée pour toi mais je ne suis qu’une personne qui a piraté tes infos et je vais te faire voir des vertes et des pas mûres!"
echo -e "\n"
sleep 2 

#DEUXIEME CHOIX
 
echo -e "\n-----------\nA.OH MON DIEU C’EST HORRIBLE!\nB.ah ok. De toute façon je n’ai rien à caché\nC.Des vertes et des pas mûres? Sérieusement fréro?\n-----------"
A="OH MON DIEU C’EST HORRIBLE!"
B="De toute façon je n’ai rien à caché"
C="... Des vertes et des pas mûres? Sérieusement fréro?"
choix=-1
while [ $choix != A ] && [ $choix != B ] && [ $choix != C ]; do
    read -p "votre choix (A/B/C): " choix 
    case $choix in
        A ) echo "YOU : OH MON DIEU C’EST HORRIBLE!" ;;
        B ) echo "YOU : De toute façon je n’ai rien à caché" ;;
        C ) echo "YOU : ... Des vertes et des pas mûres? Sérieusement fréro?" ;;
        * ) echo "Hey, tu as pas marqué un choix existant! Tapes A,B ou C";; #Voici le cas par defaut si l'utilisateur de rentre pas la bonne chose
    esac
done
sleep 2
echo -e "\n"

A="HACKER : et oui! Pour échapper à cette terrible épreuve, tu dois te lancer dans l’aventure!"
B="HACKER : Ah ouais grosse tête, pas de soucis je vais envoyer à la planète entière tes messages à ton ex? C'est bien ce que je penses!"
C="HACKER : … Rien que pour ça je vais te faire subir une terrible épreuve rien que pour te faire chier!"
reponse=$(troischoix $choix $A $B $C)

echo "$reponse"
sleep 2 
echo -e "\n"

echo -e "Pour résumer, aujourd’hui ne pas avoir un ordinateur dans le domaine du travail nous semble invraisemblable, tu es l’ingénieur de demain et deux choix s’offrent à toi : \nle système libre et le système non libre,\nLinux ou Windows,\nla liberté ou le confort,\nun chemin rempli d’épines mais glorieux ou un chemin facile mais piégeux!\n\nQue vas tu choisir euh… C’est quoi ton nom déjà?"
sleep 2
read -p "votre prénom? :" nom
sleep 2 
echo -e "\n"
echo "YOU : $nom, tu pues quand même un peu comme hackeur à ne pas savoir ça.."
sleep 2
echo -e "\n"
echo -e "HACKEUR : HEHO $nom ne m'insultes pas!, Bref! Décides toi c'est là où tout ce décide!\nA.système libre\nB. ou non?"
sleep 2 
choix=-1
while [ $choix != A ] && [ $choix != B ]; do
    read -p "votre choix (A/B) " choix
    case $choix in
        A ) echo "système libre" ;;
        B ) echo "système non libre" ;;
        * ) echo "Desole je vous comprend pas, les choix sont A et B." ;; #Voici le cas par defaut si l'utilisateur de rentre pas la bonne chose
    esac
done

A="système libre"
B="système non libre"
reponse=$(deuxchoix $choix $A $B)
sleep 2 
echo -e "HACKER : $reponse!Très bien $nom!  Fais bien attention à toi, tes photos de l'integ en dépendent!"

if [ $choix = "A" ]
then
	./chapitre1L.sh
else 
	./chapitre1P.sh
fi
